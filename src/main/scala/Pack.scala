import scala.io.Source

case class Product(id: Int, price: Int, vol: Int, weight: Int)
case class Result(price: Int, weight: Int, product: List[Int]) extends Ordered[Result] {
  override def compare(that: Result): Int =
    if (this.price == that.price) that.weight - this.weight else this.price - that.price
}


object Pack extends App {

  val (length, width, height) = (45, 30, 35)
  val V = length * width * height
  val lines = Source.fromResource("products.csv").getLines()
  val products = lines
    .map(l => l.split(","))
    .map(l => l.map(_.toInt))
    .filter(p => p(2) <= length && p(3) <= width && p(4) <= height)
    .map(l => Product(l(0), l(1), l(2) * l(3) * l(4), l(5)))
    .toArray
    .sortBy(_.vol)
    .reverse
  val N = products.length

  var dp = new Array[Result](V + 1)
  dp(0) = Result(0, 0, Nil)

  for (i <- 0 until N) {
    val newDp = dp.clone()
    for (j <- products(i).vol to V) {
      val t = dp(j - products(i).vol)
      if (t != null && (
        dp(j) == null || t.price + products(i).price > dp(j).price ||
          (t.price + products(i).price == dp(j).price && t.weight + products(i).weight < dp(j).weight))) {
        newDp(j) = Result(t.price + products(i).price, t.weight + products(i).weight, i :: t.product)
      }
    }
    dp = newDp
  }
  val result = dp.filter(_ != null).max
  println(result)
  println(result.product.foldLeft(0)((t, p) => t + products(p).id))
}